#include "MuonCondAlg/CscCondDbAlg.h"
#include "MuonCondAlg/MdtCondDbAlg.h"
#include "MuonCondAlg/RpcCondDbAlg.h"
#include "MuonCondAlg/TgcCondDbAlg.h"
#include "MuonCondAlg/MuonAlignmentErrorDbAlg.h"

DECLARE_COMPONENT( CscCondDbAlg )
DECLARE_COMPONENT( MdtCondDbAlg )
DECLARE_COMPONENT( RpcCondDbAlg )
DECLARE_COMPONENT( TgcCondDbAlg )
DECLARE_COMPONENT( MuonAlignmentErrorDbAlg )
